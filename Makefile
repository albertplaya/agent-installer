.PHONY: up
up:
	docker-compose up -d

.PHONY: bash
bash:
	docker-compose exec zintay-agent /bin/bash

.PHONY: down
down:
	docker-compose down

.PHONY: clean
clean: ## Clean up build artifacts
	docker-compose down -v
