Install guide
------------------

1. Install docker
```
apt-get install docker.io
apt-get install docker-compose
```

2. Download public git repository
```
git clone 
```

3. Add configuration into agent-installer/configuration/config.json

Configuration example
---------------------
````
{
  "storage_source": {
    "type": "mysql",
    "host": "us-east-2.rds.amazonaws.com",
    "user": "user",
    "password": "pwd",
    "database": "test",
    "ssl": {
        "key": "key.pem",
        "cert": "cert.pem",
        "ca": "ca.pem"`
    }
  },
  "storage_destination": {
    "type": "amazon",
    "region": "us-east-2",
    "aws-key": "AWS KEY",
    "aws-secret": "AWS SECRET",
    "bucket": "dafoo",
    "path": "storage/"
  }
}
